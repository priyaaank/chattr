package chattr.com.chattr.components;

import android.os.AsyncTask;

import java.util.ArrayList;
import java.util.List;

import chattr.com.chattr.domain.models.ChattrMessage;

public class MessageCurator {

	private static final String TAG = MessageCurator.class.getSimpleName();
	private List<CurationObserver> observersList = new ArrayList<>();
	private LogWrapper logWrapper = new LogWrapper();

	public void registerObserver(CurationObserver observer) {
		if(observersList.contains(observer)) return;
		observersList.add(observer);
	}

	public void deregisterObserver(CurationObserver observer) {
		if(observersList.contains(observer)) observersList.remove(observer);
	}

	public void curateMessage(ChattrMessage message) {
		logWrapper.info(TAG, "Message curation triggered for: " + message.getMessageText());
		new CurationTask().execute(message);
	}

	protected void notify(ChattrMessage message) {
		for(CurationObserver observer : observersList) {
			observer.curationComplete(message);
		}
	}

	private class CurationTask extends AsyncTask<ChattrMessage, Void, ChattrMessage> {

		@Override
		protected ChattrMessage doInBackground(ChattrMessage... params) {
			ChattrMessage message = params[0];
			message.curate();
			return message;
		}

		@Override
		protected void onPostExecute(ChattrMessage message) {
			logWrapper.info(TAG, "Message curation finished for: " + message.getMessageText());
			MessageCurator.this.notify(message);
		}

	}

	public interface CurationObserver {
		void curationComplete(ChattrMessage message);
	}

}
