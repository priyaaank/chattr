package chattr.com.chattr.components;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.ref.WeakReference;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import chattr.com.chattr.domain.models.ChattrMessage;

public class TitleResolverTask {

	private static final String CHARSET_NAME = "UTF-8";
	private static String TAG = TitleResolverTask.class.getSimpleName();
	private WeakReference<TitleResolutionObserver> observer;
	private String regexExpression = "<title>(.*?)</title>";
	private Pattern pattern = Pattern.compile(regexExpression);
	private LogWrapper logWrapper = new LogWrapper();

	public void resolveTitle(ChattrMessage message, String url) {
		new ResolverTask(url, message).execute(url);
	}

	public void registerObserver(TitleResolutionObserver observer) {
		this.observer = new WeakReference<>(observer);
	}

	private class ResolverTask extends AsyncTask<String, Void, String> {

		private static final int BUFFER_LENGTH = 2000;
		private static final int READ_TIMEOUT = 15000;
		private static final int CONNECT_TIMEOUT = 20000;
		private static final String USER_AGENT = "Mozilla/5.0 (Linux; Android 4.4.4; Nexus 5 Build/KTU84P) " +
				"AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.114 Mobile Safari/537.36";
		public static final int MAX_BUFFERED_READS = 5;
		private String url;
		private ChattrMessage message;

		ResolverTask(String url, ChattrMessage message) {
			this.url = url;
			this.message = message;
		}

		@Override
		protected String doInBackground(String... params) {
			logWrapper.debug(TAG, "Starting title resolution for url " + params[0]);
			InputStream inputStream = null;
			int bufferLength = BUFFER_LENGTH;

			try {
				URL url = new URL(params[0]);
				inputStream = openInputStreamForUrl(inputStream, url);
				String title = extractTitleFromInputStream(inputStream, bufferLength);
				return title;
			} catch(IOException muie) {
				logWrapper.error(TAG, "Error occurred while working with stream : " + muie.getMessage());
			} finally {
				closeInputStream(inputStream);
			}
			return null;
		}

		@Override
		protected void onPostExecute(String title) {
			logWrapper.debug(TAG, "Task execution finish. Resolved to title " + title);
			if(title != null && title.length() > 0) {
				if(observer != null && observer.get() != null) observer.get().resolvedUrlToTitle(message, url, title);
			}
		}

		@NonNull
		private String extractTitleFromInputStream(InputStream is, int bufferLength) throws IOException {
			int index = 0;
			String contentAsString = "";
			String title;
			while((title = match(contentAsString)) == null) {
				index++;
				contentAsString += readContentsToBuffer(is, bufferLength);
				if(index > MAX_BUFFERED_READS) {
					logWrapper.error(TAG, "Exiting title resolution prematurely. It hasn't been resolved in " +
							"first 10k bytes; possibly due to response encoding.");
					return null;
				}
			}
			return title;
		}

		private InputStream openInputStreamForUrl(InputStream is, URL url) throws IOException {
			logWrapper.debug(TAG, "Opening connection to url " + url.toString());
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setReadTimeout(READ_TIMEOUT);
			conn.setConnectTimeout(CONNECT_TIMEOUT);
			conn.setRequestProperty("Accept-Charset", "UTF-8");
			conn.setRequestProperty("User-Agent", USER_AGENT);
			conn.setRequestMethod("GET");
			conn.setDoInput(true);
			conn.setInstanceFollowRedirects(true);
			conn.connect();
			is = conn.getInputStream();
			return is;
		}

		private void closeInputStream(InputStream is) {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					logWrapper.error(TAG, "Error occurred while closing stream" + e.getMessage());
				}
			}
		}

		private String match(String response) {
			Matcher matcher = pattern.matcher(response);
			if(matcher.find()) {
				String title = matcher.group(1);
				return title;
			}
			return null;
		}

		private String readContentsToBuffer(InputStream stream, int length) throws IOException {
			Reader reader;
			reader = new InputStreamReader(stream, CHARSET_NAME);
			char[] buffer = new char[length];
			reader.read(buffer);
			return new String(buffer);
		}
	}

	public interface TitleResolutionObserver {
		void resolvedUrlToTitle(ChattrMessage message, String url, String title);
	}
}
