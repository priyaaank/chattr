package chattr.com.chattr.components;

import android.util.Log;

public class LogWrapper {

	public void error(String tag, String message) {
		Log.e(tag, message);
	}

	public void debug(String tag, String message) {
		Log.d(tag, message);
	}

	public void info(String tag, String message) {
		Log.i(tag, message);
	}

}
