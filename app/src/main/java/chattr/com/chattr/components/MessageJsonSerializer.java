package chattr.com.chattr.components;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import chattr.com.chattr.domain.models.ChattrUrl;
import chattr.com.chattr.domain.models.Emoticon;
import chattr.com.chattr.domain.models.Mention;
import chattr.com.chattr.domain.models.MessageNode;

public class MessageJsonSerializer {

	private static String TAG = MessageJsonSerializer.class.getSimpleName();
	private String[] emoticons;
	private UrlSummary[] urls;
	private String[] mentions;

	public MessageJsonSerializer(List<Emoticon> emoticonList, List<ChattrUrl> urls, List<Mention> mentions) {
		this.emoticons = extractValues(emoticonList);
		this.mentions = extractValues(mentions);
		this.urls = buildUrls(urls);
	}

	private UrlSummary[] buildUrls(List<ChattrUrl> urls) {
		List<UrlSummary> urlsToReturn = new ArrayList<>();
		if(urls == null || urls.size() == 0) return new UrlSummary[0];

		for(ChattrUrl url : urls) {
			urlsToReturn.add(new UrlSummary(url.getValue(), url.getTitle()));
		}
		return urlsToReturn.toArray(new UrlSummary[urlsToReturn.size()]);
	}

	private String[] extractValues(List<? extends MessageNode> nodeList) {
		List<String> values = new ArrayList<>();
		if(nodeList == null || nodeList.size() == 0) return new String[0];

		for(MessageNode node : nodeList) {
			values.add(node.getValue());
		}
		return values.toArray(new String[values.size()]);
	}

	public String toJson() {
		String jsonString = new Gson().toJson(this);
		new LogWrapper().info(TAG, "Generated json for message: " + jsonString);
		return jsonString;
	}

	class UrlSummary {
		private String url;
		private String title;

		public UrlSummary(String url, String title) {
			this.url = url;
			this.title = title;
		}
	}

}
