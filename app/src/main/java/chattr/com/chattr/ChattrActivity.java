package chattr.com.chattr;

import android.os.Bundle;
import android.support.v4.app.Fragment;

import chattr.com.chattr.views.chat.ChatFragment;
import chattr.com.chattr.views.SingleFragmentActivity;

public class ChattrActivity extends SingleFragmentActivity {

	private static String FRAGMENT_TAG = "ChatFragment";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	protected String getFragmentTag() {
		return FRAGMENT_TAG;
	}

	@Override
	protected Fragment getFragment() {
		return ChatFragment.newInstance();
	}

	@Override
	protected String getTitleForScreen() {
		return getString(R.string.app_name);
	}
}
