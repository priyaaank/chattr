package chattr.com.chattr.views.chat;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.Vector;

import chattr.com.chattr.components.LogWrapper;
import chattr.com.chattr.components.TitleResolverTask;
import chattr.com.chattr.domain.models.ChattrMessage;
import chattr.com.chattr.domain.models.ChattrUrl;
import chattr.com.chattr.domain.models.MessageNode;
import chattr.com.chattr.domain.models.NodeType;

public class ChatMessageAdapter extends BaseAdapter implements TitleResolverTask.TitleResolutionObserver {

	private static String TAG = ChatMessageAdapter.class.getSimpleName();
	private final LayoutInflater inflater;
	private TitleResolverTask resolver;
	private Vector<ChattrMessage> messageList;
	private ViewCreator viewCreator;
	private LogWrapper logWrapper = new LogWrapper();

	public ChatMessageAdapter(Context context, ViewCreator viewCreator) {
		this(viewCreator, LayoutInflater.from(context.getApplicationContext()), new TitleResolverTask());
	}

	public ChatMessageAdapter(ViewCreator viewCreator, LayoutInflater inflater, TitleResolverTask urlTitleResolver) {
		this.inflater = inflater;
		this.resolver = urlTitleResolver;
		this.messageList = new Vector<>();
		updateMsgViewCreator(viewCreator);
		urlTitleResolver.registerObserver(this);
	}

	@Override
	public int getCount() {
		return messageList.size();
	}

	@Override
	public ChattrMessage getItem(int position) {
		return messageList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ChattrMessage message = messageList.get(position);
		enqueueUrlResolutionFor(message);
		return viewCreator.generateView(convertView, parent, inflater, message);
	}

	public void addMessage(ChattrMessage message) {
		this.messageList.add(message);
		notifyDataSetChanged();
	}

	public void updateMsgViewCreator(ViewCreator viewCreator) {
		this.viewCreator = viewCreator;
		notifyDataSetChanged();
	}

	@Override
	public void resolvedUrlToTitle(ChattrMessage message, String url, String title) {
		logWrapper.debug(TAG, "URL resolved to title [" + url + "," + title + "]");
		int index = messageList.indexOf(message);
		if(index == -1) return;

		ChattrMessage messageToUpdate = messageList.get(index);
		messageToUpdate.updateUrlWithTitle(url, title);
		notifyDataSetChanged();
	}

	private void enqueueUrlResolutionFor(ChattrMessage message) {
		for(MessageNode node : message.getRichNodes()) {
			if(node.getType() == NodeType.URL && !((ChattrUrl)node).isTitleSet()) {
				logWrapper.debug(TAG, "Enqueuing url to resolve to title " + node.getValue());
				resolver.resolveTitle(message, node.getValue());
			}
		}
	}

}
