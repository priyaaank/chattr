package chattr.com.chattr.views.chat;

import chattr.com.chattr.R;
import chattr.com.chattr.components.LogWrapper;
import chattr.com.chattr.components.MessageCurator;
import chattr.com.chattr.domain.models.ChattrMessage;

public class ChatPresenter implements MessageCurator.CurationObserver {

	private static final String TAG = ChatPresenter.class.getSimpleName();
	private ChatView chatView;
	private MessageCurator messageCurator;
	private LogWrapper logWrapper;

	public ChatPresenter(ChatView chatView, MessageCurator messageCurator) {
		this.logWrapper = new LogWrapper();
		this.chatView = chatView;
		this.messageCurator = messageCurator;
	}

	public void onCreateView() {
		togglePrettyFormat();
		chatView.updateSendStateTo(false);
	}

	public void sendMessage() {
		logWrapper.info(TAG, "Message has been sent");
		ChattrMessage message = new ChattrMessage(chatView.currentTypedMessage());
		messageCurator.curateMessage(message);
	}

	@Override
	public void curationComplete(ChattrMessage message) {
		logWrapper.debug(TAG, "Curation is complete for message");
		chatView.updateMessageView(message);
		chatView.clearMessageBox();
	}

	public void onPause() {
		messageCurator.deregisterObserver(this);
	}

	public void onResume() {
		messageCurator.registerObserver(this);
	}

	public void onTextChanged(CharSequence text) {
		boolean isTextMissing = (text == null || text.length() == 0);
		chatView.updateSendStateTo(!isTextMissing);
	}

	public void msgFormatChanged(boolean isChecked) {
		logWrapper.debug(TAG, "Message format changed to" + (isChecked ? "Pretty" : "Raw"));
		if(isChecked) {
			togglePrettyFormat();
			return;
		}

		toggleRawFormat();
	}

	private void toggleRawFormat() {
		chatView.changeMsgFormatToRaw();
		chatView.updateInfoMsgWith(R.string.showing_raw_output);
	}

	private void togglePrettyFormat() {
		chatView.changeMsgFormatToPretty();
		chatView.updateInfoMsgWith(R.string.showing_pretty_output);
	}

}
