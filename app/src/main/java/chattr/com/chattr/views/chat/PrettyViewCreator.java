package chattr.com.chattr.views.chat;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.apmem.tools.layouts.FlowLayout;

import java.util.HashMap;
import java.util.Map;

import chattr.com.chattr.R;
import chattr.com.chattr.components.LogWrapper;
import chattr.com.chattr.domain.models.ChattrMessage;
import chattr.com.chattr.domain.models.ChattrUrl;
import chattr.com.chattr.domain.models.MessageNode;
import chattr.com.chattr.domain.models.NodeType;

public class PrettyViewCreator implements ViewCreator {

	private static final String TAG = PrettyViewCreator.class.getSimpleName();
	private LogWrapper logWrapper = new LogWrapper();

	private Map<NodeType, ViewStrategy> nodeTypeViewMap = new HashMap<NodeType, ViewStrategy>() {
		{
			put(NodeType.EMOTICON, new EmoticonViewStrategy());
			put(NodeType.MENTION, new MentionViewStrategy());
			put(NodeType.URL, new UrlViewStrategy());
			put(NodeType.PLAIN, new PlainViewStrategy());
		}
	};
	private Context applicationContext;

	public PrettyViewCreator(Context applicationContext) {
		this.applicationContext = applicationContext;
	}

	@Override
	public View generateView(View convertView, ViewGroup parent, LayoutInflater inflater, ChattrMessage message) {
		View viewToRender = convertView;

		if(viewToRender == null) {
			viewToRender = inflater.inflate(R.layout.chat_message_layout, parent, false);
		}

		FlowLayout messageContainer = (FlowLayout) viewToRender.findViewById(R.id.chat_message_flow_container);
		messageContainer.removeAllViews();
		for(MessageNode node : message.getRichNodes()) {
			View nodeView = getViewFor(inflater, node);
			messageContainer.addView(nodeView);
		}

		return viewToRender;
	}

	private View getViewFor(LayoutInflater layoutInflater, MessageNode node) {
		ViewStrategy viewCreator = nodeTypeViewMap.get(node.getType());
		return viewCreator.getView(layoutInflater, node);
	}

	private class PlainViewStrategy implements ViewStrategy {

		int viewResourceId = R.layout.plain_word_layout;

		@Override
		public View getView(LayoutInflater inflater, MessageNode node) {
			logWrapper.debug(TAG, "Generating pretty view for plain text " + node.getValue());
			View plainWordView = inflater.inflate(viewResourceId, null);
			TextView wordContainer = (TextView) plainWordView.findViewById(R.id.plain_word_container);
			wordContainer.setText(node.getValue());
			return plainWordView;
		}
	}

	private class EmoticonViewStrategy implements ViewStrategy {

		int viewResourceId = R.layout.emoticon_layout;

		@Override
		public View getView(LayoutInflater inflater, MessageNode node) {
			logWrapper.debug(TAG, "Generating pretty view for emoticon " + node.getValue());
			View emoticonContainer = inflater.inflate(viewResourceId, null);
			return emoticonContainer;
		}
	}

	private class UrlViewStrategy implements ViewStrategy {

		int viewResourceId = R.layout.url_layout;

		@Override
		public View getView(LayoutInflater inflater, MessageNode node) {
			logWrapper.debug(TAG, "Generating pretty view for url " + node.getValue());
			View urlContainer = inflater.inflate(viewResourceId, null);
			TextView urlValue = (TextView) urlContainer.findViewById(R.id.url_container);
			urlValue.setText(((ChattrUrl)node).getTitle());
			urlContainer.setOnClickListener(openUrl(node.getValue()));
			return urlContainer;
		}

		private View.OnClickListener openUrl(final String url) {
			return new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
					browserIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					applicationContext.startActivity(browserIntent);
				}
			};
		}

	}

	private class MentionViewStrategy implements ViewStrategy {

		int viewResourceId = R.layout.mention_layout;

		@Override
		public View getView(LayoutInflater inflater, MessageNode node) {
			logWrapper.debug(TAG, "Generating pretty view for mention " + node.getValue());
			View mentionContainer = inflater.inflate(viewResourceId, null);
			TextView mentionValue = (TextView) mentionContainer.findViewById(R.id.mention_container);
			mentionValue.setText(node.getValue());
			return mentionContainer;
		}
	}

	interface ViewStrategy {

		View getView(LayoutInflater inflater, MessageNode node);

	}

}
