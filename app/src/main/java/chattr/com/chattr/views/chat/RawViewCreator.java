package chattr.com.chattr.views.chat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.apmem.tools.layouts.FlowLayout;

import chattr.com.chattr.R;
import chattr.com.chattr.components.LogWrapper;
import chattr.com.chattr.domain.models.ChattrMessage;

public class RawViewCreator implements ViewCreator {

	private static String TAG = RawViewCreator.class.getSimpleName();
	private LogWrapper logWrapper = new LogWrapper();

	@Override
	public View generateView(View convertView, ViewGroup parent, LayoutInflater inflater, ChattrMessage message) {
		logWrapper.debug(TAG, "Generating raw view for " + message);
		View viewToRender = convertView;

		if(viewToRender == null) {
			viewToRender = inflater.inflate(R.layout.chat_message_layout, parent, false);
		}

		View rawJsonView = inflater.inflate(R.layout.plain_word_layout, null);
		TextView plainTextContainer = (TextView) rawJsonView.findViewById(R.id.plain_word_container);
		FlowLayout messageContainer = (FlowLayout) viewToRender.findViewById(R.id.chat_message_flow_container);
		messageContainer.removeAllViews();

		plainTextContainer.setText(message.asJson());
		messageContainer.addView(rawJsonView);

		return viewToRender;
	}
}
