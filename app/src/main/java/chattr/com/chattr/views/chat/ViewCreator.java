package chattr.com.chattr.views.chat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import chattr.com.chattr.domain.models.ChattrMessage;

public interface ViewCreator {

	View generateView(View convertView, ViewGroup parent, LayoutInflater inflater, ChattrMessage message);

}
