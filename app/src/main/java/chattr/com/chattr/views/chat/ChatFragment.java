package chattr.com.chattr.views.chat;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnTextChanged;
import chattr.com.chattr.R;
import chattr.com.chattr.components.MessageCurator;
import chattr.com.chattr.domain.models.ChattrMessage;

public class ChatFragment extends Fragment implements ChatView {

	private ChatPresenter presenter;

	@Bind(R.id.message_edit_text)
	EditText messageBox;

	@Bind(R.id.message_list)
	ListView messageList;

	@Bind(R.id.msh_format_info)
	TextView formatToggleInfoText;

	@Bind(R.id.send_message_button)
	Button sendButton;

	private ChatMessageAdapter chatMessageAdapter;

	public static ChatFragment newInstance() {
		return new ChatFragment();
	}

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.chat_view, container, false);
		ButterKnife.bind(this, view);
		this.presenter = new ChatPresenter(this, new MessageCurator());
		chatMessageAdapter = new ChatMessageAdapter(getActivity().getApplicationContext(), new PrettyViewCreator(getActivity().getApplicationContext()));
		this.messageList.setAdapter(chatMessageAdapter);
		presenter.onCreateView();
		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		presenter.onResume();
	}

	@Override
	public void onPause() {
		super.onPause();
		presenter.onPause();
	}

	@OnClick(R.id.send_message_button)
	public void sendMessage(View v) {
		this.presenter.sendMessage();
	}

	@OnCheckedChanged(R.id.msg_format_toggle)
	public void onCheckedChange(boolean isChecked) {
		presenter.msgFormatChanged(isChecked);
	}

	@OnTextChanged(R.id.message_edit_text)
	public void onTextChanged(CharSequence text) {
		presenter.onTextChanged(text);
	}

	@Override
	public String currentTypedMessage() {
		return messageBox.getText().toString();
	}

	@Override
	public void updateMessageView(ChattrMessage message) {
		chatMessageAdapter.addMessage(message);
	}

	@Override
	public void clearMessageBox() {
		messageBox.setText("");
	}

	@Override
	public void changeMsgFormatToPretty() {
		chatMessageAdapter.updateMsgViewCreator(new PrettyViewCreator(getActivity().getApplicationContext()));
	}

	@Override
	public void changeMsgFormatToRaw() {
		chatMessageAdapter.updateMsgViewCreator(new RawViewCreator());
	}

	@Override
	public void updateInfoMsgWith(@StringRes int infoMessage) {
		formatToggleInfoText.setText(getString(infoMessage));
	}

	@Override
	public void updateSendStateTo(boolean isEnabled) {
		sendButton.setEnabled(isEnabled);
	}
}
