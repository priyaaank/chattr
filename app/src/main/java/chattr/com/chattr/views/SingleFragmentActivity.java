package chattr.com.chattr.views;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import chattr.com.chattr.R;

public abstract class SingleFragmentActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_container);
		attachFragment();
	}

	private void attachFragment() {
		getSupportFragmentManager().
				beginTransaction().
				replace(R.id.main_container, getFragment(), getFragmentTag()).
				commit();
	}

	@Override
	public void onBackPressed() {
		this.finish();
	}

	protected abstract String getFragmentTag();

	protected abstract Fragment getFragment();

	protected abstract String getTitleForScreen();
}