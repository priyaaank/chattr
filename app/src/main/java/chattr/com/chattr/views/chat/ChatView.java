package chattr.com.chattr.views.chat;

import android.support.annotation.StringRes;

import chattr.com.chattr.domain.models.ChattrMessage;

public interface ChatView {

	String currentTypedMessage();

	void updateMessageView(ChattrMessage message);

	void clearMessageBox();

	void changeMsgFormatToPretty();

	void changeMsgFormatToRaw();

	void updateInfoMsgWith(@StringRes int infoMessage);

	void updateSendStateTo(boolean isEnabled);
}
