package chattr.com.chattr.domain.models;

public class Mention implements MessageNode {

	private String name;

	public Mention(final String name) {
		this.name = name;
	}

	@Override
	public String getValue() {
		return name;
	}

	@Override
	public NodeType getType() {
		return NodeType.MENTION;
	}
}
