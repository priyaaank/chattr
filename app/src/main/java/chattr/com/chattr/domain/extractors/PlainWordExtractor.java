package chattr.com.chattr.domain.extractors;

import java.util.List;
import java.util.regex.Pattern;

import chattr.com.chattr.domain.models.ChattrMessage;
import chattr.com.chattr.domain.models.PlainText;

public class PlainWordExtractor implements EntityExtractor<PlainText> {

	private final String plainWordExpression = "([\\S]+)";
	private final Pattern pattern = Pattern.compile(plainWordExpression);
	private final CoreRegexMatcher<PlainText> coreRegexMatcher;

	public PlainWordExtractor() {
		this.coreRegexMatcher = new CoreRegexMatcher<>(this);
	}

	@Override
	public List<PlainText> extract(String message) {
		return coreRegexMatcher.extract(message);
	}

	@Override
	public void enrich(ChattrMessage message) {
		//do nothing
	}

	@Override
	public Pattern getPattern() {
		return pattern;
	}

	@Override
	public PlainText getNodeFor(String matchingValue) {
		return new PlainText(matchingValue);
	}
}
