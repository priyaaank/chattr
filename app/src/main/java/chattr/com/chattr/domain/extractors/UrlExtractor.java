package chattr.com.chattr.domain.extractors;

import java.util.List;
import java.util.regex.Pattern;

import chattr.com.chattr.domain.models.ChattrMessage;
import chattr.com.chattr.domain.models.ChattrUrl;

public class UrlExtractor implements EntityExtractor<ChattrUrl> {

	private final String urlPattern = "((https?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|])";
	private final Pattern pattern = Pattern.compile(urlPattern);
	private final CoreRegexMatcher<ChattrUrl> coreRegexMatcher;

	public UrlExtractor() {
		this.coreRegexMatcher = new CoreRegexMatcher<>(this);
	}

	@Override
	public List<ChattrUrl> extract(String message) {
		return coreRegexMatcher.extract(message);
	}

	@Override
	public void enrich(ChattrMessage message) {
		message.updateUrls(coreRegexMatcher.extract(message.getMessageText()));
	}

	@Override
	public Pattern getPattern() {
		return pattern;
	}

	@Override
	public ChattrUrl getNodeFor(String matchingValue) {
		return new ChattrUrl(matchingValue);
	}
}
