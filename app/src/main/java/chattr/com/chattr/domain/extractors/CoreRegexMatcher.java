package chattr.com.chattr.domain.extractors;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import chattr.com.chattr.domain.models.MessageNode;

public class CoreRegexMatcher<T extends MessageNode> {

	private EntityExtractor<T> entityExtractor;

	public CoreRegexMatcher(EntityExtractor<T> entityExtractor) {
		this.entityExtractor = entityExtractor;
	}

	public List<T> extract(String message) {
		List<T> nodes = new ArrayList<>();
		Matcher matcher = entityExtractor.getPattern().matcher(message);
		while (matcher.find()) {
			T node = entityExtractor.getNodeFor(matcher.group(1));
			nodes.add(node);
		}
		return nodes;
	}

}
