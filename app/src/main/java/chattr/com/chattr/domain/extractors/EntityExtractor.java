package chattr.com.chattr.domain.extractors;

import java.util.List;
import java.util.regex.Pattern;

import chattr.com.chattr.domain.models.ChattrMessage;
import chattr.com.chattr.domain.models.MessageNode;

public interface EntityExtractor<T extends MessageNode> {

	List<T> extract(String message);

	void enrich(ChattrMessage message);

	Pattern getPattern();

	T getNodeFor(String matchingValue);

}
