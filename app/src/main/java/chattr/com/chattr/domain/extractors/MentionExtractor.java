package chattr.com.chattr.domain.extractors;

import java.util.List;
import java.util.regex.Pattern;

import chattr.com.chattr.domain.models.ChattrMessage;
import chattr.com.chattr.domain.models.Mention;

public class MentionExtractor implements EntityExtractor<Mention> {

	private final String mentionsExpression = "\\@(\\w+)";
	private final Pattern pattern = Pattern.compile(mentionsExpression);
	private final CoreRegexMatcher<Mention> coreRegexMatcher;

	public MentionExtractor() {
		this.coreRegexMatcher = new CoreRegexMatcher<>(this);
	}

	@Override
	public List<Mention> extract(String message) {
		return coreRegexMatcher.extract(message);
	}

	@Override
	public void enrich(ChattrMessage message) {
		message.updateMentions(coreRegexMatcher.extract(message.getMessageText()));
	}

	@Override
	public Pattern getPattern() {
		return pattern;
	}

	@Override
	public Mention getNodeFor(String matchingValue) {
		return new Mention(matchingValue);
	}
}
