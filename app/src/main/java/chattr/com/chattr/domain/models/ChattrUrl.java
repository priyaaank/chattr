package chattr.com.chattr.domain.models;

public class ChattrUrl implements MessageNode {

	private static final int TITLE_MAX_LENGTH = 50;
	public static final String EMPTY_STRING = "";
	private String url;
	private String title;
	private boolean isTitleSet;

	public ChattrUrl(String url) {
		this.url = url;
		this.title = url;
		this.isTitleSet = false;
	}

	@Override
	public String getValue() {
		return url;
	}

	@Override
	public NodeType getType() {
		return NodeType.URL;
	}

	public void setTitle(String title) {
		this.title = title;
		this.isTitleSet = true;
	}

	public String getTitle() {
		if(url == null) return EMPTY_STRING;
		String displayTitle = this.isTitleSet ? title : url;
		return displayTitle.length() <= TITLE_MAX_LENGTH ?
				displayTitle : displayTitle.substring(0, TITLE_MAX_LENGTH) + "...";
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		ChattrUrl chattrUrl = (ChattrUrl) o;

		return url.equals(chattrUrl.url);
	}

	@Override
	public int hashCode() {
		return url.hashCode();
	}

	public boolean isTitleSet() {
		return isTitleSet;
	}

}
