package chattr.com.chattr.domain.models;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import chattr.com.chattr.components.LogWrapper;
import chattr.com.chattr.components.MessageJsonSerializer;
import chattr.com.chattr.domain.extractors.EmoticonExtractor;
import chattr.com.chattr.domain.extractors.EntityExtractor;
import chattr.com.chattr.domain.extractors.MentionExtractor;
import chattr.com.chattr.domain.extractors.PlainWordExtractor;
import chattr.com.chattr.domain.extractors.UrlExtractor;

public class ChattrMessage {

	public static final String WHITESPACE = " ";
	private final EntityExtractor[] extractors;

	private UUID messageId;
	private String messageText;
	private List<Emoticon> emoticons;
	private List<ChattrUrl> urls;
	private List<Mention> mentions;
	private List<MessageNode> richNodes;

	public ChattrMessage(String messageText, EntityExtractor...entityExtractors) {
		this.messageId = UUID.randomUUID();
		this.extractors = entityExtractors;
		this.messageText = messageText;
		this.richNodes = new ArrayList<>();
	}

	public ChattrMessage(String messageText) {
		this(messageText, new UrlExtractor(), new MentionExtractor(), new EmoticonExtractor(), new PlainWordExtractor());
	}

	public String getMessageText() {
		return messageText;
	}

	public void curate() {
		enrichSelf();
		populateRichNodes();
	}

	public List<MessageNode> getRichNodes() {
		return richNodes;
	}

	public void updateUrls(List<ChattrUrl> chattrUrls) {
		this.urls = chattrUrls;
	}

	public void updateMentions(List<Mention> mentions) {
		this.mentions = mentions;
	}

	public void updateEmoticons(List<Emoticon> emoticons) {
		this.emoticons = emoticons;
	}

	public String asJson() {
		//currently well under 100ms but for message without size limits, this should be async
		//and cached.
		return new MessageJsonSerializer(this.emoticons, this.urls, this.mentions).toJson();
	}

	@Override
	public boolean equals(Object o) {
		return o instanceof ChattrMessage && (o == this || (this.messageId.equals(((ChattrMessage) o).messageId)));
	}

	@Override
	public int hashCode() {
		return messageId.hashCode();
	}

	public void updateUrlWithTitle(String url, String title) {
		ChattrUrl chattrUrl = new ChattrUrl(url);
		updateRichUrlNode(chattrUrl, title);
		updateUrlNode(chattrUrl, title);
	}

	private void populateRichNodes() {
		for(String word : messageText.split(WHITESPACE)) {
			classifyWordWithExtractors(word);
		}
	}

	private void classifyWordWithExtractors(String word) {
		for(EntityExtractor extractor : extractors) {
			List extract;

			if((extract = extractor.extract(word)) != null && extract.size() > 0) {
				richNodes.addAll(extract);
				break;
			}
		}
	}

	private void enrichSelf() {
		for(EntityExtractor extractor : extractors) {
			extractor.enrich(this);
		}
	}

	private void updateRichUrlNode(ChattrUrl chattrUrl, String title) {
		int index = richNodes.indexOf(chattrUrl);
		if(index == -1) return;
		((ChattrUrl) richNodes.get(index)).setTitle(title);
	}

	private void updateUrlNode(ChattrUrl chattrUrl, String title) {
		int index = urls.indexOf(chattrUrl);
		if(index == -1) return;
		urls.get(index).setTitle(title);
	}

}
