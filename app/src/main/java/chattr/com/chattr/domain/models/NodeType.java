package chattr.com.chattr.domain.models;

public enum NodeType {

	PLAIN,
	EMOTICON,
	URL,
	MENTION

}
