package chattr.com.chattr.domain.models;

public class PlainText implements MessageNode {

	private String text;

	public PlainText(String text) {
		this.text = text;
	}

	@Override
	public String getValue() {
		return text;
	}

	@Override
	public NodeType getType() {
		return NodeType.PLAIN;
	}
}

