package chattr.com.chattr.domain.models;

public class Emoticon implements MessageNode {

	private String code;

	public Emoticon(String code) {
		this.code = code;
	}

	@Override
	public String getValue() {
		return code;
	}

	@Override
	public NodeType getType() {
		return NodeType.EMOTICON;
	}
}
