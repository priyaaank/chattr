package chattr.com.chattr.domain.extractors;

import java.util.List;
import java.util.regex.Pattern;

import chattr.com.chattr.domain.models.ChattrMessage;
import chattr.com.chattr.domain.models.Emoticon;

public class EmoticonExtractor implements EntityExtractor<Emoticon> {

	private final String emoticonsExpression = "\\(([a-z]{1,15})\\)";
	private final Pattern pattern = Pattern.compile(emoticonsExpression);
	private final CoreRegexMatcher<Emoticon> coreRegexMatcher;

	public EmoticonExtractor() {
		this.coreRegexMatcher = new CoreRegexMatcher<>(this);
	}

	@Override
	public List<Emoticon> extract(String message) {
		return coreRegexMatcher.extract(message);
	}

	@Override
	public void enrich(ChattrMessage message) {
		message.updateEmoticons(coreRegexMatcher.extract(message.getMessageText()));
	}

	@Override
	public Pattern getPattern() {
		return pattern;
	}

	@Override
	public Emoticon getNodeFor(String matchingValue) {
		return new Emoticon(matchingValue);
	}

}
