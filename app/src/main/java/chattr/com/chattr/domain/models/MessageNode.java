package chattr.com.chattr.domain.models;

public interface MessageNode {

	String getValue();

	NodeType getType();

}
