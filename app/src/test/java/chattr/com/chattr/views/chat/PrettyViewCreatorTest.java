package chattr.com.chattr.views.chat;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.apmem.tools.layouts.FlowLayout;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;

import chattr.com.chattr.R;
import chattr.com.chattr.domain.models.ChattrMessage;
import chattr.com.chattr.domain.models.ChattrUrl;
import chattr.com.chattr.domain.models.Emoticon;
import chattr.com.chattr.domain.models.Mention;
import chattr.com.chattr.domain.models.MessageNode;
import chattr.com.chattr.domain.models.PlainText;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PrettyViewCreatorTest {

	@Mock
	private LayoutInflater inflater;
	@Mock
	private ViewGroup parent;
	@Mock
	private View chatLayout;
	@Mock
	private FlowLayout flowContainer;
	@Mock
	private ChattrMessage message;
	@Mock
	private View emoticonLayout;
	@Mock
	private ImageView emoticonImageView;
	@Mock
	private View urlLayout;
	@Mock
	private TextView urlTextView;
	@Mock
	private View mentionLayout;
	@Mock
	private TextView mentionTextView;
	@Mock
	private View plainTextLayout;
	@Mock
	private TextView plainTextView;
	@Mock
	private Context context;

	private PrettyViewCreator prettyViewCreator;

	@Before
	public void setUp() throws Exception {
		prettyViewCreator = new PrettyViewCreator(context);

		when(inflater.inflate(R.layout.chat_message_layout, parent, false)).thenReturn(chatLayout);
		when(chatLayout.findViewById(R.id.chat_message_flow_container)).thenReturn(flowContainer);
		when(inflater.inflate(R.layout.emoticon_layout, null)).thenReturn(emoticonLayout);
		when(emoticonLayout.findViewById(R.id.emoticon_container)).thenReturn(emoticonImageView);
		when(inflater.inflate(R.layout.url_layout, null)).thenReturn(urlLayout);
		when(urlLayout.findViewById(R.id.url_container)).thenReturn(urlTextView);
		when(inflater.inflate(R.layout.mention_layout, null)).thenReturn(mentionLayout);
		when(mentionLayout.findViewById(R.id.mention_container)).thenReturn(mentionTextView);
		when(inflater.inflate(R.layout.plain_word_layout, null)).thenReturn(plainTextLayout);
		when(plainTextLayout.findViewById(R.id.plain_word_container)).thenReturn(plainTextView);
	}

	@Test
	public void testThatForEmoticonNodeAllExistingSubViewsOfFlowContainerAreRemoved() throws Exception {
		when(message.getRichNodes()).thenReturn(new ArrayList<MessageNode>() {{ add(new Emoticon("hello")); }});

		prettyViewCreator.generateView(null, parent, inflater, message);

		verify(flowContainer).removeAllViews();
	}

	@Test
	public void testThatForEmoticonNodeEmoticonLayoutIsCreated() throws Exception {
		when(message.getRichNodes()).thenReturn(new ArrayList<MessageNode>() {{ add(new Emoticon("hello")); }});

		prettyViewCreator.generateView(null, parent, inflater, message);

		verify(inflater).inflate(R.layout.emoticon_layout, null);
	}

	@Test
	public void testThatForEmoticonNodeEmoticonLayoutIsAddedToFlowContainer() throws Exception {
		when(message.getRichNodes()).thenReturn(new ArrayList<MessageNode>() {{ add(new Emoticon("hello")); }});

		prettyViewCreator.generateView(null, parent, inflater, message);

		verify(flowContainer).addView(emoticonLayout);
	}


	@Test
	public void testThatForUrlNodeAllExistingSubViewsOfFlowContainerAreRemoved() throws Exception {
		when(message.getRichNodes()).thenReturn(new ArrayList<MessageNode>() {{ add(new ChattrUrl("http://bit.ly/1")); }});

		prettyViewCreator.generateView(null, parent, inflater, message);

		verify(flowContainer).removeAllViews();
	}

	@Test
	public void testThatForUrlNodeUrlLayoutIsCreated() throws Exception {
		when(message.getRichNodes()).thenReturn(new ArrayList<MessageNode>() {{ add(new ChattrUrl("http://bit.ly/1")); }});

		prettyViewCreator.generateView(null, parent, inflater, message);

		verify(inflater).inflate(R.layout.url_layout, null);
	}

	@Test
	public void testThatForUrlNodeUrlValueIsSetToTextView() throws Exception {
		when(message.getRichNodes()).thenReturn(new ArrayList<MessageNode>() {{ add(new ChattrUrl("http://bit.ly/1")); }});

		prettyViewCreator.generateView(null, parent, inflater, message);

		verify(urlTextView).setText("http://bit.ly/1");
	}

	@Test
	public void testThatForUrlNodeOnClickListenerIsSet() throws Exception {
		when(message.getRichNodes()).thenReturn(new ArrayList<MessageNode>() {{
			add(new ChattrUrl("http://bit.ly/1"));
		}});

		prettyViewCreator.generateView(null, parent, inflater, message);

		verify(urlLayout).setOnClickListener(any(View.OnClickListener.class));
	}

		@Test
	public void testThatForEmoticonNodeUrlLayoutIsAddedToFlowContainer() throws Exception {
		when(message.getRichNodes()).thenReturn(new ArrayList<MessageNode>() {{ add(new ChattrUrl("http://bit.ly/1")); }});

		prettyViewCreator.generateView(null, parent, inflater, message);

		verify(flowContainer).addView(urlLayout);
	}

	@Test
	public void testThatForMentionNodeLayoutIsCreated() throws Exception {
		when(message.getRichNodes()).thenReturn(new ArrayList<MessageNode>() {{ add(new Mention("bob")); }});

		prettyViewCreator.generateView(null, parent, inflater, message);

		verify(inflater).inflate(R.layout.mention_layout, null);
	}

	@Test
	public void testThatForMentionNodeValueIsSetToTextView() throws Exception {
		when(message.getRichNodes()).thenReturn(new ArrayList<MessageNode>() {{ add(new Mention("bob")); }});

		prettyViewCreator.generateView(null, parent, inflater, message);

		verify(mentionTextView).setText("bob");
	}

	@Test
	public void testThatForMentionNodeLayoutIsAddedToFlowContainer() throws Exception {
		when(message.getRichNodes()).thenReturn(new ArrayList<MessageNode>() {{ add(new Mention("bob")); }});

		prettyViewCreator.generateView(null, parent, inflater, message);

		verify(flowContainer).addView(mentionLayout);
	}


	@Test
	public void testThatForPlainTextNodeLayoutIsCreated() throws Exception {
		when(message.getRichNodes()).thenReturn(new ArrayList<MessageNode>() {{ add(new PlainText("simple")); }});

		prettyViewCreator.generateView(null, parent, inflater, message);

		verify(inflater).inflate(R.layout.plain_word_layout, null);
	}

	@Test
	public void testThatForPlainTextNodeValueIsSetToTextView() throws Exception {
		when(message.getRichNodes()).thenReturn(new ArrayList<MessageNode>() {{ add(new PlainText("simple")); }});

		prettyViewCreator.generateView(null, parent, inflater, message);

		verify(plainTextView).setText("simple");
	}

	@Test
	public void testThatFoPlainTextNodeLayoutIsAddedToFlowContainer() throws Exception {
		when(message.getRichNodes()).thenReturn(new ArrayList<MessageNode>() {{ add(new PlainText("simple")); }});

		prettyViewCreator.generateView(null, parent, inflater, message);

		verify(flowContainer).addView(plainTextLayout);
	}
}