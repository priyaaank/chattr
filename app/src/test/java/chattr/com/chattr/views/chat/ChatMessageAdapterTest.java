package chattr.com.chattr.views.chat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import chattr.com.chattr.components.TitleResolverTask;
import chattr.com.chattr.domain.models.ChattrMessage;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class ChatMessageAdapterTest {

	private ChatMessageAdapter chatMessageAdapter;

	@Mock
	private ViewCreator viewCreator;
	@Mock
	private LayoutInflater inflater;
	@Mock
	private ViewGroup parent;
	@Mock
	private View convertView;
	@Mock
	private TitleResolverTask titleResolver;

	@Before
	public void setUp() throws Exception {
		chatMessageAdapter = new ChatMessageAdapter(viewCreator, inflater, titleResolver);
	}

	@Test
	public void testThatByDefaultMessagesShouldNotBePresent() throws Exception {
		assertEquals(0, chatMessageAdapter.getCount());
	}

	@Test
	public void testThatPositionShouldBeReturnedAsId() throws Exception {
		assertEquals(1534, chatMessageAdapter.getItemId(1534));
	}

	@Test
	public void testThatCountShouldBeReturnedWhenMessagesPresent() throws Exception {
		ChattrMessage message = mock(ChattrMessage.class);

		chatMessageAdapter.addMessage(message);

		assertEquals(1, chatMessageAdapter.getCount());
	}

	@Test
	public void testThatViewForMessageShouldBeObtainedFromViewCreator() throws Exception {
		ChattrMessage message = mock(ChattrMessage.class);
		chatMessageAdapter.addMessage(message);

		chatMessageAdapter.getView(0, convertView, parent);

		verify(viewCreator).generateView(convertView, parent, inflater, message);
	}

	@Test
	public void testThatUpdatedViewCreatorIsUsedForView() throws Exception {
		ChattrMessage message = mock(ChattrMessage.class);
		chatMessageAdapter.addMessage(message);
		ViewCreator alternateViewCreator = mock(ViewCreator.class);

		chatMessageAdapter.updateMsgViewCreator(alternateViewCreator);
		chatMessageAdapter.getView(0, convertView, parent);

		verify(alternateViewCreator).generateView(convertView, parent, inflater, message);
	}

	@Test
	public void testThatItemIsReturnedByPosition() throws Exception {
		ChattrMessage message = mock(ChattrMessage.class);

		chatMessageAdapter.addMessage(message);

		assertEquals(message, chatMessageAdapter.getItem(0));
	}

	@Test
	public void testThatWhenRichNodesContainAUrlNodeTitleResolutionIsQueued() throws Exception {
		ChattrMessage message = new ChattrMessage("message with http://bit.ly url");
		message.curate();

		chatMessageAdapter.addMessage(message);
		chatMessageAdapter.getView(0, convertView, parent);

		verify(titleResolver).resolveTitle(message, "http://bit.ly");
	}

	@Test
	public void testThatWhenTitleHasAlreadyBeenSetOnceResolutionIsNotQueued() throws Exception {
		ChattrMessage message = new ChattrMessage("message with http://bit.ly url");
		message.curate();
		message.updateUrlWithTitle("http://bit.ly", "Bitly");

		chatMessageAdapter.addMessage(message);
		chatMessageAdapter.getView(0, convertView, parent);

		verify(titleResolver, never()).resolveTitle(any(ChattrMessage.class), any(String.class));
	}

	@Test
	public void testWhenTitleIsResolvedTitleIsUpdatedOnMessage() throws Exception {
		ChattrMessage message = mock(ChattrMessage.class);

		chatMessageAdapter.addMessage(message);
		chatMessageAdapter.resolvedUrlToTitle(message, "http://bit.ly/123", "Watermelon");

		verify(message).updateUrlWithTitle("http://bit.ly/123", "Watermelon");
	}

	@Test
	public void testWhenTitleIsResolvedButMessageIsRemovedItIsNotUpdated() throws Exception {
		ChattrMessage message = mock(ChattrMessage.class);

		chatMessageAdapter.resolvedUrlToTitle(message, "http://bit.ly/123", "Watermelon");

		verify(message, never()).updateUrlWithTitle(any(String.class), any(String.class));
	}
}