package chattr.com.chattr.views.chat;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.apmem.tools.layouts.FlowLayout;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import chattr.com.chattr.R;
import chattr.com.chattr.domain.models.ChattrMessage;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class RawViewCreatorTest {

	@Mock
	private LayoutInflater inflater;
	@Mock
	private ViewGroup parent;
	@Mock
	private ChattrMessage message;
	@Mock
	private View chatLayout;
	@Mock
	private View plainWordLayout;
	@Mock
	private TextView plainWordTextView;
	@Mock
	private FlowLayout flowContainer;

	private RawViewCreator rawViewCreator;

	@Before
	public void setUp() throws Exception {
		rawViewCreator = new RawViewCreator();

		when(message.asJson()).thenReturn("{\"awesome\":\"json\"}");

		when(inflater.inflate(R.layout.chat_message_layout, parent, false)).thenReturn(chatLayout);
		when(inflater.inflate(R.layout.plain_word_layout, null)).thenReturn(plainWordLayout);
		when(plainWordLayout.findViewById(R.id.plain_word_container)).thenReturn(plainWordTextView);
		when(chatLayout.findViewById(R.id.chat_message_flow_container)).thenReturn(flowContainer);
	}

	@Test
	public void testThatAllSubViewsAreRemovedFromFlowLayoutWhileConstructingView() throws Exception {
		rawViewCreator.generateView(null, parent, inflater, message);

		verify(flowContainer).removeAllViews();
	}

	@Test
	public void testThatSerializedJsonIsSetAsTextWhileConstructingView() throws Exception {
		rawViewCreator.generateView(null, parent, inflater, message);

		verify(plainWordTextView).setText("{\"awesome\":\"json\"}");
	}

	@Test
	public void testJsonTextViewContainerIsAddedToFlowLayout() throws Exception {
		rawViewCreator.generateView(null, parent, inflater, message);

		verify(flowContainer).addView(plainWordLayout);
	}

	@Test
	public void testThatRowLayoutIsNotReCreatedWhenAvailable() throws Exception {
		rawViewCreator.generateView(chatLayout, parent, inflater, message);

		verify(inflater, never()).inflate(R.layout.chat_message_layout, parent, false);
	}
}