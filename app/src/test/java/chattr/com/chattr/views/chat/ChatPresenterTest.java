package chattr.com.chattr.views.chat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import chattr.com.chattr.R;
import chattr.com.chattr.components.MessageCurator;
import chattr.com.chattr.domain.models.ChattrMessage;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ChatPresenterTest {

	@Mock
	private ChatView chatView;
	@Mock
	private MessageCurator messageCurator;
	@Captor
	private ArgumentCaptor<ChattrMessage> messageCaptor;

	private ChatPresenter presenter;

	@Before
	public void setUp() throws Exception {
		presenter = new ChatPresenter(chatView, messageCurator);
	}

	@Test
	public void testThatSendButtonIsDisabledOnCreateView() throws Exception {
		presenter.onCreateView();

		verify(chatView).updateSendStateTo(false);
	}

	@Test
	public void testThatMsgFormatIsSetToPrettyOnCreateView() throws Exception {
		presenter.onCreateView();

		verify(chatView).changeMsgFormatToPretty();
	}

	@Test
	public void testThatToggleInoMsgIndicatesPrettyViewMode() throws Exception {
		presenter.onCreateView();

		verify(chatView).updateInfoMsgWith(R.string.showing_pretty_output);
	}

	@Test
	public void testThatMessageCurationIsTriggeredOnSendMessage() throws Exception {
		when(chatView.currentTypedMessage()).thenReturn("hello @bob");

		presenter.sendMessage();

		verify(messageCurator).curateMessage(messageCaptor.capture());
		assertEquals("hello @bob", messageCaptor.getValue().getMessageText());
	}

	@Test
	public void testThatOnCurationCompletionMessageIsUpdated() throws Exception {
		ChattrMessage message = new ChattrMessage("some message");

		presenter.curationComplete(message);

		verify(chatView).updateMessageView(message);
	}

	@Test
	public void testThatOnCurationCompletionTypedMessageClearedFromBox() throws Exception {
		ChattrMessage message = new ChattrMessage("some message");

		presenter.curationComplete(message);

		verify(chatView).clearMessageBox();
	}

	@Test
	public void testThatOnPausePresenterDeRegisterItselfAsObserver() throws Exception {
		presenter.onPause();

		verify(messageCurator).deregisterObserver(presenter);
	}

	@Test
	public void testThatOnResumePresenterDeRegisterItselfAsObserver() throws Exception {
		presenter.onResume();

		verify(messageCurator).registerObserver(presenter);
	}

	@Test
	public void testThatOnMsgFormatToggleToRawInfoMessageIsUpdated() throws Exception {
		presenter.msgFormatChanged(false);

		verify(chatView).updateInfoMsgWith(R.string.showing_raw_output);
	}

	@Test
	public void testThatOnMsgFormatToggleToRawMessageFormatIsChanged() throws Exception {
		presenter.msgFormatChanged(false);

		verify(chatView).changeMsgFormatToRaw();
	}

	@Test
	public void testThatOnMsgFormatToggleToPrettyInfoMessageIsUpdated() throws Exception {
		presenter.msgFormatChanged(true);

		verify(chatView).updateInfoMsgWith(R.string.showing_pretty_output);
	}

	@Test
	public void testThatOnMsgFormatToggleToPrettyMessageFormatIsChanged() throws Exception {
		presenter.msgFormatChanged(true);

		verify(chatView).changeMsgFormatToPretty();
	}

	@Test
	public void testThatSendButtonStatusIsDisabledwhenNoTextTyped() throws Exception {
		presenter.onTextChanged("");

		verify(chatView).updateSendStateTo(false);
	}

	@Test
	public void testThatSendButtonStatusIsEnabledwhenTextTyped() throws Exception {
		presenter.onTextChanged("some text");

		//then
		verify(chatView).updateSendStateTo(true);
	}
}