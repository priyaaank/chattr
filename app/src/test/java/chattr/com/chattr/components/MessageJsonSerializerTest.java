package chattr.com.chattr.components;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import chattr.com.chattr.domain.models.ChattrUrl;
import chattr.com.chattr.domain.models.Emoticon;
import chattr.com.chattr.domain.models.Mention;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class MessageJsonSerializerTest {

	private List<Emoticon> emoticons;
	private List<ChattrUrl> urls;
	private List<Mention> mentions;

	@Mock
	private LogWrapper logWrapper;

	@Before
	public void setUp() throws Exception {
		emoticons = new ArrayList<Emoticon>() {{ add(new Emoticon("allthings")); }};
		urls = new ArrayList<ChattrUrl>() {{ add(new ChattrUrl("http://google.com")); }};
		mentions = new ArrayList<Mention>() {{ add(new Mention("jonsnow")); }};
	}

	@Test
	public void testThatSerializedObjectIsCreatedContainingAllNodeTypes() throws Exception {
		//given
		MessageJsonSerializer serializer = new MessageJsonSerializer(emoticons, urls, mentions);

		//when
		String jsonString = serializer.toJson();

		//then
		assertEquals("{\"emoticons\":[\"allthings\"],\"urls\":[{\"url\":\"http://google.com\"," +
				"\"title\":\"http://google.com\"}],\"mentions\":[\"jonsnow\"]}", jsonString);
	}

	@Test
	public void testThatSerializedObjectIsCreatedContainingOnlyEmoticons() throws Exception {
		//given
		urls.clear();
		mentions.clear();
		MessageJsonSerializer serializer = new MessageJsonSerializer(emoticons, urls, mentions);

		//when
		String jsonString = serializer.toJson();

		//then
		assertEquals("{\"emoticons\":[\"allthings\"],\"urls\":[],\"mentions\":[]}", jsonString);
	}


	@Test
	public void testThatSerializedObjectIsCreatedContainingOnlyUrl() throws Exception {
		//given
		emoticons.clear();
		mentions.clear();
		MessageJsonSerializer serializer = new MessageJsonSerializer(emoticons, urls, mentions);

		//when
		String jsonString = serializer.toJson();

		//then
		String expectedJson = "{\"emoticons\":[],\"urls\":[{\"url\":\"http://google.com\"," +
				"\"title\":\"http://google.com\"}],\"mentions\":[]}";
		assertEquals(expectedJson, jsonString);
	}

	@Test
	public void testThatSerializedObjectIsCreatedContainingOnlyMentions() throws Exception {
		//given
		emoticons.clear();
		urls.clear();
		MessageJsonSerializer serializer = new MessageJsonSerializer(emoticons, urls, mentions);

		//when
		String jsonString = serializer.toJson();

		//then
		assertEquals("{\"emoticons\":[],\"urls\":[],\"mentions\":[\"jonsnow\"]}", jsonString);
	}

	@Test
	public void testThatSerializedObjectIsCreatedContainingNoMessageNodes() throws Exception {
		//given
		urls.clear();
		mentions.clear();
		emoticons.clear();
		MessageJsonSerializer serializer = new MessageJsonSerializer(emoticons, urls, mentions);

		//when
		String jsonString = serializer.toJson();

		//then
		assertEquals("{\"emoticons\":[],\"urls\":[],\"mentions\":[]}", jsonString);
	}

	@Test
	public void testThatSerializedObjectIsCreatedWithMissingMessageNodes() throws Exception {
		//given
		MessageJsonSerializer serializer = new MessageJsonSerializer(null, null, null);

		//when
		String jsonString = serializer.toJson();

		//then
		assertEquals("{\"emoticons\":[],\"urls\":[],\"mentions\":[]}", jsonString);
	}

}