package chattr.com.chattr.components;

import android.os.AsyncTask;
import android.support.annotation.NonNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import chattr.com.chattr.domain.models.ChattrMessage;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class MessageCuratorTest {

	@Mock
	private AsyncTask<ChattrMessage, Void, ChattrMessage> mockCurationTask;
	@Mock
	private MessageCurator.CurationObserver observer;

	private MessageCurator messageCurator;
	private ChattrMessage message;

	@Before
	public void setUp() throws Exception {
		message = new ChattrMessage("you know nothing @jon (snow)");
		messageCurator = new TestableMessageCurator();
	}

	@Test
	public void testThatObserversAreNotifiedOfCurationCompletion() throws Exception {
		//given
		messageCurator.registerObserver(observer);

		//when
		messageCurator.notify(message);

		//then
		verify(observer).curationComplete(message);
	}

	@Test
	public void testThatMultipleObserversAreNotifiedOfCompletion() throws Exception {
		//given
		MessageCurator.CurationObserver additionalObserver = mock(MessageCurator.CurationObserver.class);
		messageCurator.registerObserver(observer);
		messageCurator.registerObserver(additionalObserver);

		//when
		messageCurator.notify(message);

		//then
		verify(observer).curationComplete(message);
		verify(additionalObserver).curationComplete(message);
	}

	@Test
	public void testThatObserversAreNotNotifiedWhenDeRegistered() throws Exception {
		//given
		messageCurator.registerObserver(observer);

		//when
		messageCurator.deregisterObserver(observer);
		messageCurator.notify(message);

		//then
		verify(observer, never()).curationComplete(message);
	}

	@Test
	public void testThatNotificationToObserverTriggeredWhenExecuted() throws Exception {
		//given
		messageCurator.registerObserver(observer);

		//when
		messageCurator.notify(message);

		//then
		verify(observer).curationComplete(message);
	}

	@NonNull
	private Answer<Void> notifyObserversAction() {
		return new Answer<Void>() {
			@Override
			public Void answer(InvocationOnMock invocation) throws Throwable {
				messageCurator.notify(message);
				return null;
			}
		};
	}

	private class TestableMessageCurator extends MessageCurator {

		@Override
		public void notify(ChattrMessage message) {
			super.notify(message);
		}

	}
}