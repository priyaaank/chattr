package chattr.com.chattr.domain.models;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class EmoticonTest {

	private Emoticon emoticon = new Emoticon("allthings");

	@Test
	public void testThatCodeValueIsReturnedWhenPresent() throws Exception {
		assertEquals("allthings", emoticon.getValue());
	}

	@Test
	public void testThatNodeTypeIsEmoticon() throws Exception {
		assertEquals(NodeType.EMOTICON, emoticon.getType());
	}

}