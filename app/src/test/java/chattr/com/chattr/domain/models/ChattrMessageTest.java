package chattr.com.chattr.domain.models;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.fail;

public class ChattrMessageTest {

	private String richMessage = "Hey @Bob (hi). Check out http://bit.ly/12345";
	private ChattrMessage message = new ChattrMessage(richMessage);

	@Test
	public void testThatMessageTextCanBeObtained() throws Exception {
		assertEquals(richMessage, message.getMessageText());
	}

	@Test
	public void testThatMessageCurationExtractCorrectCountOfMentions() throws Exception {
		//when
		message.curate();
		List<MessageNode> mentions = getNodesOfType(NodeType.MENTION, message.getRichNodes());

		//then
		assertEquals(1, mentions.size());
	}

	@Test
	public void testThatMessageCurationPopulatesAllMentionsRichNodes() throws Exception {
		//when
		message.curate();
		List<MessageNode> mentions = getNodesOfType(NodeType.MENTION, message.getRichNodes());

		//then
		assertEquals("Bob", mentions.get(0).getValue());
	}

	@Test
	public void testThatMessageCurationExtractCorrectCountOfUrls() throws Exception {
		//when
		message.curate();
		List<MessageNode> urls = getNodesOfType(NodeType.URL, message.getRichNodes());

		//then
		assertEquals(1, urls.size());
	}

	@Test
	public void testThatMessageCurationPopulatesAllUrlRichNodes() throws Exception {
		//when
		message.curate();
		List<MessageNode> urls = getNodesOfType(NodeType.URL, message.getRichNodes());

		//then
		assertEquals("http://bit.ly/12345", urls.get(0).getValue());
	}

	@Test
	public void testThatMessageCurationExtractCorrectCountOfEmoticons() throws Exception {
		//when
		message.curate();
		List<MessageNode> emoticons = getNodesOfType(NodeType.EMOTICON, message.getRichNodes());

		//then
		assertEquals(1, emoticons.size());
	}

	@Test
	public void testThatMessageCurationPopulatesAllEmoticonsRichNodes() throws Exception {
		//when
		message.curate();
		List<MessageNode> emoticons = getNodesOfType(NodeType.EMOTICON, message.getRichNodes());

		//then
		assertEquals("hi", emoticons.get(0).getValue());
	}

	@Test
	public void testThatMessageCurationExtractCorrectCountOfPlainTexts() throws Exception {
		//when
		message.curate();
		List<MessageNode> plainTexts = getNodesOfType(NodeType.PLAIN, message.getRichNodes());

		//then
		assertEquals(3, plainTexts.size());
	}

	@Test
	public void testThatMessageCurationPopulatesAllPlainTextRichNodes() throws Exception {
		//when
		message.curate();
		List<MessageNode> plainTexts = getNodesOfType(NodeType.PLAIN, message.getRichNodes());

		//then
		assertEquals("Hey", plainTexts.get(0).getValue());
		assertEquals("Check", plainTexts.get(1).getValue());
		assertEquals("out", plainTexts.get(2).getValue());
	}

	@Test
	public void testThatMessageNodesAreExtractedDuringCuration() throws Exception {
		//when
		message.curate();
		String serializedJson = message.asJson();

		//then
		assertEquals("{\"emoticons\":[\"hi\"],\"urls\":[{\"url\":\"http://bit.ly/12345\"," +
				"\"title\":\"http://bit.ly/12345\"}],\"mentions\":[\"Bob\"]}", serializedJson);
	}

	@Test
	public void testThatMessageUrlCanBeUpdated() throws Exception {
		//given
		List<ChattrUrl> urls = new ArrayList<ChattrUrl>(){{ add(new ChattrUrl("http://bit.ly/wow")); }};

		//when
		message.updateUrls(urls);
		String serializedJson = message.asJson();

		//then
		assertEquals("{\"emoticons\":[],\"urls\":[{\"url\":\"http://bit.ly/wow\"," +
				"\"title\":\"http://bit.ly/wow\"}],\"mentions\":[]}", serializedJson);
	}

	@Test
	public void testThatMessageEmoticonsCanBeUpdated() throws Exception {
		//given
		List<Emoticon> emoticons = new ArrayList<Emoticon>(){{ add(new Emoticon("allthings")); }};

		//when
		message.updateEmoticons(emoticons);
		String serializedJson = message.asJson();

		//then
		assertEquals("{\"emoticons\":[\"allthings\"],\"urls\":[],\"mentions\":[]}", serializedJson);
	}

	@Test
	public void testThatMessageMentionsCanBeUpdated() throws Exception {
		//given
		List<Mention> mentions = new ArrayList<Mention>(){{ add(new Mention("Bobby")); }};

		//when
		message.updateMentions(mentions);
		String serializedJson = message.asJson();

		//then
		assertEquals("{\"emoticons\":[],\"urls\":[],\"mentions\":[\"Bobby\"]}", serializedJson);
	}

	@Test
	public void testThatUrlRichNodesAreUpdatedWithTitle() throws Exception {
		message.curate();
		message.updateUrlWithTitle("http://bit.ly/12345", "Black holes");

		int index = message.getRichNodes().indexOf(new ChattrUrl("http://bit.ly/12345"));
		ChattrUrl urlNode = (ChattrUrl) message.getRichNodes().get(index);

		assertEquals("Black holes", urlNode.getTitle());
	}

	@Test
	public void testThatUrlTitleIsPopulatedInJson() throws Exception {
		message.curate();
		message.updateUrlWithTitle("http://bit.ly/12345", "Black holes");

		String asJson = message.asJson();

		assertEquals("{\"emoticons\":[\"hi\"],\"urls\":[{\"url\":\"http://bit.ly/12345\"," +
				"\"title\":\"Black holes\"}],\"mentions\":[\"Bob\"]}", asJson);
	}

	@Test
	public void testItShouldNotThrowErrorWhenUrlNodeAreNotPresentAndUpdateAttempted()  {
		try {
			ChattrMessage message = new ChattrMessage("no url msg");
			message.curate();
			message.updateUrlWithTitle("http://bit.ly/12345", "Black holes");
		} catch(Exception ec) {
			fail("No exception should have been thrown when url not present" + ec.getMessage());
		}
	}

	private List<MessageNode> getNodesOfType(NodeType nodeType, List<MessageNode> richNodes) {
		List<MessageNode> nodes = new ArrayList<>();
		for(MessageNode node: richNodes) {
			if(node.getType() == nodeType) {
				nodes.add(node);
			}
		}
		return nodes;
	}
}