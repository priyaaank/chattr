package chattr.com.chattr.domain.models;

import org.junit.Test;

import static org.junit.Assert.*;

public class PlainTextTest {

	private PlainText plainText = new PlainText("simplevalue");

	@Test
	public void testThatNodeTypeIsPlain() throws Exception {
		assertEquals(NodeType.PLAIN, plainText.getType());
	}

	@Test
	public void testThatValueIsReturned() throws Exception {
		assertEquals("simplevalue", plainText.getValue());
	}

}