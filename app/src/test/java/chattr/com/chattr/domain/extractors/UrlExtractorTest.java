package chattr.com.chattr.domain.extractors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import chattr.com.chattr.domain.models.ChattrMessage;
import chattr.com.chattr.domain.models.ChattrUrl;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class UrlExtractorTest {

	private UrlExtractor urlExtractor;

	@Captor
	private ArgumentCaptor<List<ChattrUrl>> urlCaptor;

	@Before
	public void setUp() throws Exception {
		urlExtractor = new UrlExtractor();
	}

	@Test
	public void testThatExtractorCanExtractValidUrls() throws Exception {
		ChattrMessage messageWithUrl = new ChattrMessage("@Bob check this url https://bitbucket.org/priyaaank/chattr");

		List<ChattrUrl> chattrUrls = urlExtractor.extract(messageWithUrl.getMessageText());

		assertEquals("https://bitbucket.org/priyaaank/chattr",chattrUrls.get(0).getValue());
	}

	@Test
	public void testThatExtractorCanExtractMultipleValidUrls() throws Exception {
		String stringWithUrls = "http://bit.ly/123?a=12 and http://bit.ly/123?a=122";
		ChattrMessage messageWithUrl = new ChattrMessage(stringWithUrls);
		List<ChattrUrl> chattrUrls = urlExtractor.extract(messageWithUrl.getMessageText());

		assertEquals("http://bit.ly/123?a=12",chattrUrls.get(0).getValue());
		assertEquals("http://bit.ly/123?a=122",chattrUrls.get(1).getValue());
	}

	@Test
	public void testThatEmptyListOfUrlsIsReturnedWhenNonePresent() throws Exception {
		ChattrMessage messageWithUrl = new ChattrMessage("@Bob I don't have the url");

		List<ChattrUrl> chattrUrls = urlExtractor.extract(messageWithUrl.getMessageText());

		assertEquals(0,chattrUrls.size());
	}

	@Test
	public void testThatASpaceTerminatesAUrl() throws Exception {
		ChattrMessage messageWithUrl = new ChattrMessage("@Bob wierd url http://bit.ly/23_ 23?a=232");

		List<ChattrUrl> chattrUrls = urlExtractor.extract(messageWithUrl.getMessageText());

		assertEquals("http://bit.ly/23_",chattrUrls.get(0).getValue());
	}

	@Test
	public void testEmoticonPatternIsAnyAlphaNumValueBetweenOneToFifteenCharsInParenthesis() throws Exception {
		String regex = "((https?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|])";
		assertEquals(regex, urlExtractor.getPattern().toString());
	}

	@Test
	public void testThatNodeIsReturnedInitializedWithMatchingValue() throws Exception {
		String matchingValue = "http://bit.ly/1242?q=132";

		ChattrUrl url = urlExtractor.getNodeFor(matchingValue);

		assertEquals("http://bit.ly/1242?q=132", url.getValue());
	}

	@Test
	public void testThatMessageIsEnrichedAfterExtraction() throws Exception {
		ChattrMessage mockMessage = mock(ChattrMessage.class);
		when(mockMessage.getMessageText()).thenReturn("Check out http://bit.ly/1242?q=132");

		urlExtractor.enrich(mockMessage);

		verify(mockMessage).updateUrls(urlCaptor.capture());
		List<ChattrUrl> chattrUrls = urlCaptor.getValue();

		assertEquals("http://bit.ly/1242?q=132", chattrUrls.get(0).getValue());
	}
}