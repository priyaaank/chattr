package chattr.com.chattr.domain.extractors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import chattr.com.chattr.domain.models.ChattrMessage;
import chattr.com.chattr.domain.models.PlainText;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PlainWordExtractorTest {

	private PlainWordExtractor plainWordExtractor;

	@Before
	public void setUp() throws Exception {
		plainWordExtractor = new PlainWordExtractor();
	}

	@Test
	public void testThatExtractorCanExtractValidWords() throws Exception {
		ChattrMessage messageWithUrl = new ChattrMessage("check this example");

		List<PlainText> plainTexts = plainWordExtractor.extract(messageWithUrl.getMessageText());

		assertEquals("check",plainTexts.get(0).getValue());
		assertEquals(3,plainTexts.size());
	}

	@Test
	public void testThatExtractorReturnsEmptyListWhenNoWordsPresent() throws Exception {
		ChattrMessage messageWithUrl = new ChattrMessage("");

		List<PlainText> plainTexts = plainWordExtractor.extract(messageWithUrl.getMessageText());

		assertEquals(0,plainTexts.size());
	}

	@Test
	public void testEmoticonPatternIsAnyAlphaNumValueBetweenOneToFifteenCharsInParenthesis() throws Exception {
		String regex = "([\\S]+)";
		assertEquals(regex, plainWordExtractor.getPattern().toString());
	}

	@Test
	public void testThatNodeIsReturnedInitializedWithMatchingValue() throws Exception {
		String matchingValue = "awesome";

		PlainText plainText = plainWordExtractor.getNodeFor(matchingValue);

		assertEquals("awesome", plainText.getValue());
	}

	@Test
	public void testThatMessageIsNotEnrichedForPlainTextWords() throws Exception {
		ChattrMessage mockMessage = mock(ChattrMessage.class);
		when(mockMessage.getMessageText()).thenReturn("what is new?");

		plainWordExtractor.enrich(mockMessage);

		verify(mockMessage, never()).getMessageText();
	}

}