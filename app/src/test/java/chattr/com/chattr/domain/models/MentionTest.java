package chattr.com.chattr.domain.models;

import org.junit.Test;

import static org.junit.Assert.*;

public class MentionTest {

	private Mention mention = new Mention("James");

	@Test
	public void testThatMentionNameIsReturnedWhenPresent() throws Exception {
		assertEquals("James", mention.getValue());
	}

	@Test
	public void testThatNodeTypeIsMention() throws Exception {
		assertEquals(NodeType.MENTION, mention.getType());
	}

}