package chattr.com.chattr.domain.extractors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import chattr.com.chattr.domain.models.ChattrMessage;
import chattr.com.chattr.domain.models.Mention;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MentionExtractorTest {

	private EntityExtractor<Mention> mentionExtractor;

	@Captor
	private ArgumentCaptor<List<Mention>> mentionCaptor;

	@Before
	public void setUp() throws Exception {
		mentionExtractor = new MentionExtractor();
	}

	@Test
	public void testThatCorrectNumberOfMentionsAreIdentified() throws Exception {
		ChattrMessage messageWithValidMention = new ChattrMessage("Hey @Bob, why don't you join us for lunch?");

		List mentions = mentionExtractor.extract(messageWithValidMention.getMessageText());

		assertEquals(1, mentions.size());
	}

	@Test
	public void testThatAValidMentionIsExtractedFromAMessage() throws Exception {
		ChattrMessage messageWithValidMention = new ChattrMessage("Hey @Bob, why don't you join us for lunch?");

		List<Mention> mentions = mentionExtractor.extract(messageWithValidMention.getMessageText());

		assertEquals("Bob", mentions.get(0).getValue());
	}

	@Test
	public void testThatAllMentionsAreExtractedFromAMessage() throws Exception {
		ChattrMessage messageWithMultipleMentions = new ChattrMessage("Hey @Bob, @Lily are you coming?");

		List<Mention> mentions = mentionExtractor.extract(messageWithMultipleMentions.getMessageText());

		assertEquals("Bob", mentions.get(0).getValue());
		assertEquals("Lily", mentions.get(1).getValue());
	}

	@Test
	public void testThatAllMentionsContainingWordCharAreExtractedFromAMessage() throws Exception {
		ChattrMessage messageWithAlphaNumMentions = new ChattrMessage("Nice! @B0b, @lily_dorson, @007Bond!");

		List<Mention> mentions = mentionExtractor.extract(messageWithAlphaNumMentions.getMessageText());

		assertEquals("B0b", mentions.get(0).getValue());
		assertEquals("lily_dorson", mentions.get(1).getValue());
		assertEquals("007Bond", mentions.get(2).getValue());
	}

	@Test
	public void testThatInvalidMentionsAreIgnoredInAMessage() throws Exception {
		ChattrMessage messageWithInvalidMention = new ChattrMessage("@ Bob, there?");

		List<Mention> mentions = mentionExtractor.extract(messageWithInvalidMention.getMessageText());

		assertEquals(0, mentions.size());
	}

	@Test
	public void testThatMessageWithMentionsReturnEmptyList() throws Exception {
		ChattrMessage messageWithoutMentions = new ChattrMessage("Is everyone ok there?");

		List<Mention> mentions = mentionExtractor.extract(messageWithoutMentions.getMessageText());

		assertEquals(0, mentions.size());
	}

	@Test
	public void testMentionPatternIsAnyAlphaNumValueBetweenOneToFifteenCharsInParenthesis() throws Exception {
		assertEquals("\\@(\\w+)", mentionExtractor.getPattern().toString());
	}

	@Test
	public void testThatNodeIsReturnedInitializedWithMatchingValue() throws Exception {
		String matchingValue = "bob";

		Mention mention = mentionExtractor.getNodeFor(matchingValue);

		assertEquals("bob", mention.getValue());
	}

	@Test
	public void testThatMessageIsEnrichedAfterExtraction() throws Exception {
		ChattrMessage mockMessage = mock(ChattrMessage.class);
		when(mockMessage.getMessageText()).thenReturn("ok @bob ");

		mentionExtractor.enrich(mockMessage);

		verify(mockMessage).updateMentions(mentionCaptor.capture());
		List<Mention> mentionList = mentionCaptor.getValue();

		assertEquals("bob", mentionList.get(0).getValue());
	}

}
