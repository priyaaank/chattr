package chattr.com.chattr.domain.extractors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import chattr.com.chattr.domain.models.ChattrMessage;
import chattr.com.chattr.domain.models.Emoticon;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class EmoticonExtractorTest {

	@Captor
	private ArgumentCaptor<List<Emoticon>> emoticonsCaptor;

	private EntityExtractor<Emoticon> emoticonExtractor;

	@Before
	public void setUp() throws Exception {
		emoticonExtractor = new EmoticonExtractor();
	}

	@Test
	public void testThatAValidEmoticonIsExtracted() throws Exception {
		ChattrMessage messageWithAValidEmoticon = new ChattrMessage("(rudolph) the red nose reindeer!");

		List<Emoticon> emoticons = emoticonExtractor.extract(messageWithAValidEmoticon.getMessageText());

		assertEquals("rudolph", emoticons.get(0).getValue());
	}

	@Test
	public void testThatExtractedEmoticonCountIsCorrect() throws Exception {
		ChattrMessage messageWithAValidEmoticon = new ChattrMessage("(rudolph) the red nose reindeer!");

		List<Emoticon> emoticons = emoticonExtractor.extract(messageWithAValidEmoticon.getMessageText());

		assertEquals(1, emoticons.size());
	}

	@Test
	public void testThatUptoFifteenCharsIsAValidEmoticon() throws Exception {
		ChattrMessage messageWithAValidEmoticon = new ChattrMessage("(iamfifteenchars) is an emoticon!");

		List<Emoticon> emoticons = emoticonExtractor.extract(messageWithAValidEmoticon.getMessageText());

		assertEquals(1, emoticons.size());
		assertEquals("iamfifteenchars", emoticons.get(0).getValue());
	}

	@Test
	public void testThatMatchingPatternWithMoreThanFifteenCharsIsNotAnEmoticon() throws Exception {
		ChattrMessage messageWithAValidEmoticon = new ChattrMessage("(iamsixteencharss) is not an emoticon!");

		List<Emoticon> emoticons = emoticonExtractor.extract(messageWithAValidEmoticon.getMessageText());

		assertEquals(0, emoticons.size());
	}

	@Test
	public void testThatEmptyParenthesisDoNotQualifyAsEmoticon() throws Exception {
		ChattrMessage messageWithAValidEmoticon = new ChattrMessage("() is not an emoticon!");

		List<Emoticon> emoticons = emoticonExtractor.extract(messageWithAValidEmoticon.getMessageText());

		assertEquals(0, emoticons.size());
	}

	@Test
	public void testEmoticonPatternIsAnyAlphaNumValueBetweenOneToFifteenCharsInParenthesis() throws Exception {
		assertEquals("\\(([a-z]{1,15})\\)", emoticonExtractor.getPattern().toString());
	}

	@Test
	public void testThatNodeIsReturnedInitializedWithMatchingValue() throws Exception {
		String matchingValue = "allthings";

		Emoticon emoticon = emoticonExtractor.getNodeFor(matchingValue);

		assertEquals("allthings", emoticon.getValue());
	}

	@Test
	public void testThatMessageIsEnrichedAfterExtraction() throws Exception {
		ChattrMessage mockMessage = mock(ChattrMessage.class);
		when(mockMessage.getMessageText()).thenReturn("(laugh) my friend");

		emoticonExtractor.enrich(mockMessage);

		verify(mockMessage).updateEmoticons(emoticonsCaptor.capture());
		List<Emoticon> emoticonList = emoticonsCaptor.getValue();

		assertEquals("laugh", emoticonList.get(0).getValue());
	}

}
