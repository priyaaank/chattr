package chattr.com.chattr.domain.models;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

public class ChattrUrlTest {

	private ChattrUrl url = new ChattrUrl("http://somedummyurl.com");

	@Test
	public void testThatUrlValueCanBeObtained() throws Exception {
		assertEquals("http://somedummyurl.com", url.getValue());
	}

	@Test
	public void testThatNodeTypeIsUrl() throws Exception {
		assertEquals(NodeType.URL, url.getType());
	}

	@Test
	public void testThatTitleSetStatusIsFalseWhenTitleIsNotSet() throws Exception {
		assertFalse(url.isTitleSet());
	}

	@Test
	public void testThatTitleSetStatusIsTrueWhenTitleIsSet() throws Exception {
		url.setTitle("Wow");

		assertTrue(url.isTitleSet());
	}

	@Test
	public void testThatUrlIsReturnedAsTitleWhenTitleNotSet() throws Exception {
		String title = url.getTitle();

		assertEquals("http://somedummyurl.com", title);
	}

	@Test
	public void testThatUrlIsTruncatedAtFiftyCharsWhenReturnedAsTitle() throws Exception {
		ChattrUrl url = new ChattrUrl("http://bit.ly/12345678901234567890123456789234323420");
		String title = url.getTitle();

		assertEquals("http://bit.ly/123456789012345678901234567892343234...", title);
	}

	@Test
	public void testThatUrlIsNotTruncatedAtExactlyFiftyCharsWhenReturnedAsTitle() throws Exception {
		ChattrUrl url = new ChattrUrl("http://bit.ly/123456789012345678901234567890123456");
		String title = url.getTitle();

		assertEquals("http://bit.ly/123456789012345678901234567890123456", title);
	}

	@Test
	public void testThatTitleIsTruncatedAtExactlyFiftyCharsWhenReturnedAsTitle() throws Exception {
		ChattrUrl url = new ChattrUrl("http://bit.ly/123456789012345678901234567890123456");
		url.setTitle("thisismysuperlongtitlethatneedstobetruncatedatexactlyfiftychars");

		String title = url.getTitle();

		assertEquals("thisismysuperlongtitlethatneedstobetruncatedatexac...", title);
	}

	@Test
	public void testThatAnObjectIsEqualToSelf() throws Exception {
		ChattrUrl urlOne = new ChattrUrl("http://goo.gl");

		urlOne.setTitle("Google");

		assertEquals(urlOne, urlOne);
	}

	@Test
	public void testThatAnObjectOfDifferentTypeIsNotEqual() throws Exception {
		ChattrUrl urlOne = new ChattrUrl("http://goo.gl");
		PlainText text = new PlainText("http://goo.gl");

		assertNotEquals(text, urlOne);
	}

	@Test
	public void testThatNullIsNotEqualToAnObjectWithUrl() throws Exception {
		ChattrUrl urlOne = new ChattrUrl("http://goo.gl");

		urlOne.setTitle("Google");

		assertNotEquals(null, urlOne);
	}

	@Test
	public void testThatTwoChattrUrlsWithSameUrlDifferentTitleAreEqual() throws Exception {
		ChattrUrl urlOne = new ChattrUrl("http://goo.gl");
		ChattrUrl urlTwo = new ChattrUrl("http://goo.gl");

		urlOne.setTitle("Google");
		urlTwo.setTitle("Not Google");

		assertEquals(urlOne, urlTwo);
	}

	@Test
	public void testThatHashCodeForTwoObjectsWithSameUrlIsSame() throws Exception {
		ChattrUrl urlOne = new ChattrUrl("http://goo.gl");
		ChattrUrl urlTwo = new ChattrUrl("http://goo.gl");

		urlOne.setTitle("Google");
		urlTwo.setTitle("Not Google");

		assertEquals(urlOne.hashCode(), urlTwo.hashCode());
	}
}