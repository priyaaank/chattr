package chattr.com.chattr.domain.extractors;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;
import java.util.regex.Pattern;

import chattr.com.chattr.domain.models.MessageNode;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CoreRegexMatcherTest {

	CoreRegexMatcher<MessageNode> regexMatcher;
	private Pattern pattern = Pattern.compile("(.*)");

	@Mock
	private EntityExtractor<MessageNode> entityExtractor;

	@Before
	public void setUp() throws Exception {
		regexMatcher = new CoreRegexMatcher<>(entityExtractor);
		when(entityExtractor.getPattern()).thenReturn(pattern);
	}

	@Test
	public void testThatPatternShouldBeRequestedFromExtractor() throws Exception {
		regexMatcher.extract("some message");

		verify(entityExtractor).getPattern();
	}

	@Test
	public void testThatNodeIsRequestedForExtractedKeywords() throws Exception {
		regexMatcher.extract("some message");

		verify(entityExtractor).getNodeFor("some message");
	}

	@Test
	public void testThatConstructedNodesAreReturnedAsResult() throws Exception {
		MessageNode messageNode = mock(MessageNode.class);
		when(entityExtractor.getNodeFor("some message")).thenReturn(messageNode);

		List<MessageNode> nodes = regexMatcher.extract("some message");

		assertEquals(messageNode, nodes.get(0));
	}

	@Test
	public void testThatEmptyListIsReturnedWhenNoMatchingNodesAreFound() throws Exception {
		when(entityExtractor.getPattern()).thenReturn(Pattern.compile("blue"));

		List<MessageNode> nodes = regexMatcher.extract("some message");

		assertEquals(0, nodes.size());
	}
}