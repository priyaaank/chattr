package chattr.com.chattr;

import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import chattr.com.chattr.pages.ChatScreen;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class ChattrApplicationTest {

	@Rule
	public IntentsTestRule<ChattrActivity> mActivityRule = new IntentsTestRule<>(ChattrActivity.class);

	private ChatScreen chatScreen = new ChatScreen();
	private String message = "Hey @Emily check out this url https://github.com/priyaaank (awesome)!";

	@Test
	public void chatterRichMessagesAreRendered() {
		chatScreen.
				typeMessage(message).
				sendMessage().
				verifyMessageWasSent().
				verifyRichMessageIsDisplayed().
				verifyClickOnUrlOpensIt();
	}

	@Test
	public void chatterRawMessagesAreRendered() {
		chatScreen.
				switchToRawView().
				typeMessage(message).
				sendMessage().
				verifyMessageWasSent().
				verifyRawMessageIsDisplayed();
	}

}
