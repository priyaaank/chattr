package chattr.com.chattr.pages;

import android.app.Instrumentation;
import android.content.Intent;
import android.net.Uri;

import org.hamcrest.Matcher;

import chattr.com.chattr.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.Intents.intending;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasAction;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasData;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.allOf;

public class ChatScreen {

	public ChatScreen() {

	}

	public ChatScreen typeMessage(String message) {
		onView(withId(R.id.message_edit_text)).perform(typeText(message));
		return this;
	}

	public ChatScreen sendMessage() {
		onView(withId(R.id.send_message_button)).perform(click());
		return this;
	}

	public ChatScreen verifyMessageWasSent() {
		onView(withId(R.id.message_edit_text)).check(matches(withText("")));
		return this;
	}

	public ChatScreen verifyRichMessageIsDisplayed() {
		checkIfTextBitsIsDisplayed();
		checkIfEmotionIsDisplayed();
		checkIfUrlIsDisplayed();
		return this;
	}

	public ChatScreen switchToRawView() {
		onView(withId(R.id.msg_format_toggle)).perform(click());
		return this;
	}

	public ChatScreen verifyRawMessageIsDisplayed() {
		String rawValue = "{\"emoticons\":[\"awesome\"],\"mentions\":[\"Emily\"]," +
				"\"urls\":[{\"title\":\"priyaaank · GitHub\",\"url\":\"https://github.com/priyaaank\"}]}";
		onView(withText(rawValue)).check(matches(isDisplayed()));
		return this;
	}


	private void checkIfEmotionIsDisplayed() {
		onView(withId(R.id.emoticon_container)).check(matches(isDisplayed()));
	}

	private void checkIfUrlIsDisplayed() {
		String title = "priyaaank · GitHub";
		onView(withText(title)).check(matches(isDisplayed()));
	}

	private void checkIfTextBitsIsDisplayed() {
		String[] messageFragments = new String[] {"Hey", "Emily", "check", "out", "this", "url"};
		for(String messageFragment : messageFragments) {
			onView(withText(messageFragment)).check(matches(isDisplayed()));
		}
	}

	public ChatScreen verifyClickOnUrlOpensIt() {
		Matcher<Intent> expectedIntent = allOf(hasAction(Intent.ACTION_VIEW), hasData(Uri.parse("https://github.com/priyaaank")));
		intending(expectedIntent).respondWith(new Instrumentation.ActivityResult(0, null));
		onView(withId(R.id.url_container)).perform(click());
		intended(expectedIntent);
		return this;
	}
}
