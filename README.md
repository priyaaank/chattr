# App demo 

**Download APK file** [here](http://bit.ly/chattrcode)

![ezgif.com-video-to-gif.gif](https://bitbucket.org/repo/xzEAGa/images/3261182596-ezgif.com-video-to-gif.gif)

# Problem statement summary

Build a chat solution that uses HipChat emoticon and mention format rules and classifies them. Urls are also classified and their page title is resolved. A sample response json structure looks as follows:

```
#!json

{ 
  "emoticons":["awesome"],
  "mentions":["Emily"],
  "urls":[
    {
      "title":"priyaaank · GitHub",
      "url":"https://github.com/priyaaank"
    }
  ]
}
```

# Solution Overview
**Download APK file** [here](http://bit.ly/chattrcode)

An android native app supports min sdk version 16 to latest version 23. In addition to the raw json format of the classification, it does rendering of messages in rich style on a chat window, like hip chat application. _It doesn't have an actual chat application built in though_.

# Test Coverage
* Approx 70-75% coverage with unit test using Mockito, Junit4. Can be executed with with task `gradle test`
* Remaining 25% covered with thin layer of Feature tests using Espresso for Android. Can be executed with task `gradle connectedDebugAndroidTest`. 

# Functionality #
* Overview of the UI and functionality available in the *gif* file above.
* App currently supports toggle between raw view and rich view